

BUILD_DIR = build
TARGET = program

# List your assembly source files here. Default: Find all .S files in directory
S_SOURCES = $(wildcard *.S)

PREFIX = arm-none-eabi-

MCU = -mcpu=cortex-m0plus

# Flags to pass to the assembler
ASFLAGS = $(MCU) -g 

# Name of the linker script
LDSCRIPT = pico.ld

# Flags to pass to the linker
LDFLAGS = -T $(LDSCRIPT) -Map=$(BUILD_DIR)/$(TARGET).map --cref


S_OBJECTS	= $(foreach file,$(S_SOURCES),$(BUILD_DIR)/$(basename $(file)).o)

all : $(BUILD_DIR)/$(TARGET).elf $(BUILD_DIR)/$(TARGET).hex $(BUILD_DIR)/$(TARGET).bin

$(BUILD_DIR)/$(TARGET).elf : $(S_OBJECTS) $(LDSCRIPT)
	$(PREFIX)ld $(LDFLAGS) -o $@ $(S_OBJECTS)
	$(PREFIX)size $@

$(BUILD_DIR)/$(TARGET).hex : $(BUILD_DIR)/$(TARGET).elf
	$(PREFIX)objcopy -O ihex $< $@

$(BUILD_DIR)/$(TARGET).bin : $(BUILD_DIR)/$(TARGET).elf
	$(PREFIX)objcopy -O binary $< $@

$(S_OBJECTS) : $(BUILD_DIR)/%.o : %.S Makefile | $(BUILD_DIR)
	$(PREFIX)as $(ASFLAGS) -o $@ $<

$(BUILD_DIR):
	mkdir $@


.PHONY : gdbsrv program gdb clean

gdbsrv :
	openocd -f interface/picoprobe.cfg -f target/rp2040.cfg

program : $(BUILD_DIR)/$(TARGET).elf
	openocd -f interface/picoprobe.cfg -f target/rp2040.cfg -c "program $< verify reset exit"

gdb : $(BUILD_DIR)/$(TARGET).elf
	$(PREFIX)gdb $^

clean :
	rm -fr $(BUILD_DIR)
